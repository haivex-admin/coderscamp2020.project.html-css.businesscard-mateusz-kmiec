# Matesz Kmieć (Haivex) Portfolio

# Opis projektu
Jest to pierwszy projekt wykonany w trakcie kursu CodersCamp 2020. Projekt miał na celu sprawdzić znajomość i umiejętność wykorzystania HTML i CSS. Zadaniem było przygotowanie strony wizytówki. Posłużyłem się projektem interfejsu strony — wizytówki, wykonanym przez organizatorów kursu za pomocą Figmy: 
[Projekt można zobaczyć tutaj](https://www.figma.com/file/WHtiMfcNHt4tc7mDamNBYa/CodersCamp2020-Wizytowka?node-id=0%3A1)

# Użyte technologie:
Projekt został wykonany bez żadnych bibliotek i styli (użyłem tylko czcionek Font Awesome). Poniżej znajdują się narzędzia które, wykorzystałem:
- HTML
- CSS
- Font Awesome

# Zagadnienia
Zaprezentowałem praktyczną znajomość poniższych zagadnień związanych z HTML & CSS:
- Box-model
- Kaskadowość CSS
- Selektory CSS
- Popularne tagi HTML
- Podpięcie styli do pliku HTML
- Zapisywanie kolorów
- Stylowanie tekstu
- Wykorzystanie zewnętrznych czcionek Font Awesome
- Flexbox i CSS Grid
- Position (absolute, relative)
- Animacje
- Responsive Web Design
- Formularz ( tylko wygląd bez wysyłania formularza)

# Demo
Portfolio zostało umieszczone na Github Pages. Link do strony znajduje się poniżej.
Demo: [Kliknij tutaj](https://haivex.github.io/CodersCamp2020.Project.HTML-CSS.BusinessCard-Mateusz-Kmiec/)
